# Intro

this repo is to contribute to the static page of the spanish hackmeeting website

https://es.hackmeeting.org

# ML

to participate to the mailing list you can visit this page

https://listas.sindominio.net/mailman/listinfo/hackmeeting

# Contribute

if you want to contribute chaning the website you need to 
1. have an account on https://0xacab.org
2. know some HTML and CSS (basic)
3. install and use git to push your changes
 
Once you have that, then proceeed by checking and editing the code available here

https://0xacab.org/hackes/eshackmeeting
    
please, remember to inform the ML about the change.

